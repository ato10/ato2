<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Feed\Entities\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boxes = \Cache::remember('users', 5, function () {
            $array = [
                29 => 'bg-danger',
                4 => 'bg-success',
                11 => 'bg-info',
                25 => 'bg-warning',
            ];
            $boxes = collect();
            foreach ($array as $key => $value) {
                $box = new \App\Box;
                $cat = Category::findOrFail($key);
                $count = \DB::table('feeds')
                        ->where('category_id', $key)->count();
                $box->id = $cat->id;
                $box->name = $cat->name;
                $box->count = $count;
                $box->background = $value;
                $boxes->push($box);
            }
            return $boxes;
        });
        return view('home')->with(compact('boxes'));
    }
}
