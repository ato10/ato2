@extends('layouts.app')

@section('title')
Feeds
@endsection

@section('site_title')
Feeds
@endsection

@section('content')
    <div id="pjax-container">
    <div class="pull-left">
        <form class="form-inline" role="search" method="get" action="{!! url('feed') !!}" data-pjax>
            <input type="hidden" name="a" value="{{ request('a', '') }}">
            <input type="hidden" name="c" value="{{ request('c', '') }}">
            <div class="form-group">
                <div class="input-group">
                    <input name="q" type="text" class="form-control" id="navbar-search-input"
                            placeholder="Search" value="{!! Request::get('q', '') !!}">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-fab btn-fab-mini">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            <div class="ripple-container"></div>
                        </button>
                    </span>
                </div>
            </div>
        </form>
    </div>
    <div class="pull-right">
        <form id="form-cats" class="form-inline" role="search" method="get" action="{!! url('feed') !!}" data-pjax>
            <select name="c" class="form-control" onchange="$('#form-cats').submit();">
                <option value="">All categories</option>
                @foreach ($categories as $category)
                    @if ($category->id == request()->get('c'))
                        <option selected value="{{ $category->id }}">{{ $category->name }}</option>
                    @else
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endif
                @endforeach
            </select>
        </form>
    </div>

        <div class="clearfix"></div>
        <br>
        {!! print_filter('q') !!}
        {!! print_filter('c') !!}
        {!! print_filter('a') !!}
        <br><br>
        @if (count($feeds) > 0)
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Category</th>
                        <th>Date</th>
                        <th>Size</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($feeds as $feed)
                    <tr>
                        <td>
                            @if ($feed->content != null)
                                <a href="{{ url('/feed', $feed->slug) }}">{{ $feed->title }}</a>
                            @else
                                <a target="_blank" href="{{ $feed->link }}">{{ $feed->title }}</a>
                            @endif
                        </td>
                        <td><a href="{!! get_filter('a', $feed->author) !!}">{{ $feed->author }}</a></td>
                        <td><a data-pjax href="{!! get_filter('c', $feed->category->id) !!}">{{ $feed->category->name }}</a></td>
                        <td>{{ $feed->pubDate->format('d/m/Y H:i:s') }}</td>
                        <td class="text-right nowrap">
                            @if ($feed->enclosure_length)
                                    {!! human_filesize($feed->enclosure_length) !!}
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if ($feed->enclosure_type === 'application/x-bittorrent')
                                <a href="{!! $feed->enclosure_url !!}" target="_blank">
                                    <i class="fa fa-download" aria-hidden="true"></i>
                                </a>
                            @else
                                <a href="{!! $feed->link !!}" target="_blank"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $feeds->links() !!}
        @else
        <p>No records found.</p>
        <form target="_blank" action="https://ilcorsaronero.info/argh.php" method="get">
            <div class="form-group">
                <input class="form-control" name="search" value="{!! Request::get('q', '') !!}" type="text">
            </div>
            <input class="btn btn-default" type="submit" value="cerca" alt="Submit">
        </form>
        @endif
    </div>
@stop
