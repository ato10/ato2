@extends('layouts.app')

@section('title')
Feeds - <small>{{ $feed->title }}</small>
@endsection

@section('site_title')
Feeds - {{ $feed->title }}
@endsection

@section('content')
    <div class="item">
        {!! $feed->content !!}
    </div>
@endsection

@section('scripts')
    @parent
    <script>
    $('.panel-body:first .item:first img').addClass('wow fadeIn');
    $(document).ready(function(){
        $('.item img').addClass('img-responsive');
        $('.item iframe').width('100%');
    });
    </script>
@endsection

@section('styles')
    .codecolorer-container {
        width: 100% !important;
        margin: 0px !important;
    }
@endsection
