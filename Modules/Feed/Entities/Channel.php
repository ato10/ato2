<?php

namespace Modules\Feed\Entities;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $fillable = ['name', 'url', 'type', 'category'];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
