<?php

namespace Modules\Feed\Entities;

use Watson\Rememberable\Rememberable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Rememberable;

    public $timestamps = false;
    protected $fillable = ['id', 'name'];

    public function feeds()
    {
        return $this->hasMany('Modules\Feed\Entities\Feed');
    }
}
