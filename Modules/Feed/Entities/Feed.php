<?php

namespace Modules\Feed\Entities;

use Watson\Rememberable\Rememberable;
use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    use Rememberable;

    protected $fillable = [];
    protected $dates = [ 'pubDate' ];

    public function category()
    {
        return $this->belongsTo('Modules\Feed\Entities\Category');
    }
}
