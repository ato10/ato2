<?php

namespace Modules\Feed\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Feed\Entities\Feed;
use Illuminate\Routing\Controller;
use Modules\Feed\Entities\Category;

class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $q = $request->get('q', '');
        $a = $request->get('a', '');
        $c = $request->get('c', null);
        //\Cache::flush();
        $feeds = Feed::remember(5)
                //->with('category')
                ->with(['category' => function ($query) { $query->remember(5); }])
                ->where('title', 'like', "%$q%")
                ->where('author', 'like', "%$a%")
                ->whereHas('category', function ($query) use ($c) {
                    if ($c !== null || !empty($c)) {
                        $query->remember(5)->where('id', $c);
                    }
                })
                ->orderBy('pubDate', 'desc')->paginate(15);
        $feeds->setPath($request->fullUrlWithQuery(['page' => null]));
        $categories = Category::remember(5)->orderby('name', 'asc')->get();
        return view('feed::index')->with(compact('feeds'))->with(compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('feed::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($slug)
    {
        $feed = Feed::where('slug', $slug)->first();
        if ($feed == null) {
            \App::abort(404);
        }
        return view('feed::show')->with(compact('feed'));
        //return view('feed::show')->with(compact('item'))->withShortcodes();
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('feed::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
