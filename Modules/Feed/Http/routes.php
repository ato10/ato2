<?php

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'feed', 'namespace' => 'Modules\Feed\Http\Controllers'], function()
{
    Route::get('/', 'FeedController@index');
    Route::get('/{slug}', 'FeedController@show');
});
