<?php

use Modules\Feed\Entities\Channel;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url');
            $table->string('type');
            $table->string('category')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Channel::create([
            'name' => 'ilCorSaRoNeRo',
            'url' => 'https://ilcorsaronero.info/rss',
            'type' => 'rss20',
            'category' => null,
        ]);
        Channel::create([
            'name' => 'T.N.T. Village',
            'url' => 'http://www.tntvillage.scambioetico.org/rss.php?c=0&p=10000000000',
            'type' => 'rss20',
            'category' => null,
        ]);
        Channel::create([
            'name' => 'Skidrow Games',
            'url' => 'http://feeds.feedburner.com/skidrowgames',
            'type' => 'rss20_2',
            'category' => 11,
        ]);
        Channel::create([
            'name' => 'Skidrow Games (www.skidrow-games.com)',
            'url' => 'https://www.skidrow-games.com/category/pc-games/feed/',
            'type' => 'rss20_2',
            'category' => 11,
        ]);
        Channel::create([
            'name' => 'Skidrow Games (www.skidrow-games.com)',
            'url' => 'https://www.skidrow-games.com/category/daily-releases/feed/',
            'type' => 'rss20_2',
            'category' => 11,
        ]);
        Channel::create([
            'name' => 'Skidrow Games (www.skidrow-games.com)',
            'url' => 'https://www.skidrow-games.com/category/pc-repack/feed/',
            'type' => 'rss20_2',
            'category' => 11,
        ]);
        Channel::create([
            'name' => 'Skidrow Games (www.skidrow-games.com)',
            'url' => 'https://www.skidrow-games.com/category/game-updates/feed/',
            'type' => 'rss20_2',
            'category' => 11,
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channels');
    }
}
