<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'rollbar' => [
        'access_token' => env('ROLLBAR_TOKEN', 'e1d26feee60149bd8841a4083522e024'),
        'level' => env('ROLLBAR_LEVEL', 'error'),
    ],

    'google' => [
        'client_id' => '1035115421370-625k4eqk0nh6oc8dk7q8360kcm7vqnae.apps.googleusercontent.com',
        'client_secret' => 'ghDnQLFxA5WVR3VlZWgAMf6U',
        'redirect' => 'http://ato-ato10.rhcloud.com/login/google/callback',
    ],

];
