<?php
function get_filter($k, $v) {
    $request = request();
    if (empty($v)) {
        return $request->fullUrlWithQuery([
            'page' => null,
        ]);
    } else {
        return $request->fullUrlWithQuery([
            'page' => null,
            $k => $v,
        ]);
    }
}

function print_filter($k) {
    $request = request();
    switch ($k) {
        case 'a':
            if (empty($request->get('a', ''))) {
                return '';
            } else {
                return '<a data-pjax href="' . $request->fullUrlWithQuery(['page' => null, 'a' => null]) . '"><span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i> ' . $request->get('a') . '</span></a>';
            }
        break;
        case 'q':
            if (empty($request->get('q', ''))) {
                return '';
            } else {
                return '<a data-pjax href="' . $request->fullUrlWithQuery(['page' => null, 'q' => null]) . '"><span class="label label-primary"><i class="fa fa-times" aria-hidden="true"></i> ' . $request->get('q') . '</span></a>';
            }
        break;
        case 'c':
            if (empty($request->get('c', ''))) {
                return '';
            } else {
                $cat = \Modules\Feed\Entities\Category::findOrFail($request->get('c'));
                return '<a data-pjax href="' . $request->fullUrlWithQuery(['page' => null, 'c' => null]) . '"><span class="label label-success"><i class="fa fa-times" aria-hidden="true"></i> ' . $cat->name . '</span></a>';
            }
        break;
    }
}

function stringToColorCode($id)
{
    return '#DDD'.substr(md5($id), 0, 3);
}

function human_filesize($bytes, $decimals = 2)
{
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . ' ' . @$size[$factor];
}

function setActive($path)
{
    return request()->is($path . '*') ? ' class="active"' :  '';
}
