@extends('layouts.app')

@section('content-top')
<div class="row">
    @foreach ($boxes as $box)
        <div class="col-sm-6 col-md-3">
            <div class="panel panel-default panel-box">
                <div class="panel-body">
                    <h2>{{ $box->count }}</h2>
                    <p><a href="{!! url('feed') !!}?c={{ $box->id }}">{{ $box->name }}</a></p>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection

@section('content')
You are logged in!
@endsection
