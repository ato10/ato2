<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        {{ config('app.name', 'Laravel') }}
        @hasSection('site_title')
            | @yield('site_title')
        @endif
    </title>

    <link rel="icon" href="{!! asset('favicon.png') !!}">

    <!-- Styles -->
    <link href="{{ asset('css/loader.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <style media="screen">
        body { padding-top: 70px; }
        .nowrap { white-space: nowrap; }
        .panel-box a { color: #373E42; }
    </style>
    <style media="screen">
    @yield('styles')
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        @if (!Auth::guest())
                            <li{!! setActive('feed') !!}><a href="{{ url('feed') }}">Feeds</a></li>
                        @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    @yield('content-top')

                    <div class="panel panel-default">
                        <div class="panel-heading">@yield('title', 'Dashboard')</div>

                        <div class="panel-body">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @yield('bottom')
    </div>

    <!-- Scripts -->
    @section('scripts')
    <!--<script src="https://use.fontawesome.com/fcd3058302.js"></script>-->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script src="{{ asset('js/jquery.pjax.js') }}"></script>
    <script>
    new WOW().init();
    $('.navbar-brand:first').addClass('wow rubberBand');
    $(document).ready(function() {
        $('.loading').hide();
    });
    if ($.support.pjax) {
        $(document).on('submit', 'form[data-pjax]', function(event) {
            $.pjax.submit(event, '#pjax-container')
        });
        $(document).on('click', 'a[data-pjax]', function(event) {
            $.pjax.click(event, {container: '#pjax-container'})
        });
        $(document).on('pjax:beforeSend', function(event, xhr, settings) {
            $('.loading').show();
        });
        $(document).on('pjax:complete', function(event, xhr, textStatus, settings) {
            $('.loading').hide();
        });
    }
    </script>
    @show
    <div class="loading">Loading&#8230;</div>
</body>
</html>
